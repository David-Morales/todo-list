package ec.edu.epn.morales.david.todo_list

import android.app.Activity
import android.app.AlertDialog
import android.app.Application
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.widget.EditText
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_list_detail.*
import java.util.*

class ListDetailActivity : AppCompatActivity() {

    lateinit var listRecyclerView: RecyclerView
    lateinit var listId: String


    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("todo-list")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_detail)
        listRecyclerView= findViewById(R.id.list_child_view)
        listRecyclerView.adapter= ListSelectionRecyclerViewAdapter(ref)


        listId= intent.getStringExtra(MainActivity.INTENT_LIST_ID)



        ref.child(listId).child("list-name")
            .addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    title= dataSnapshot.value.toString()
                }
            })

        fab2.setOnClickListener {view->
            showCreateListDialog()

        }
    }

    private fun showCreateListDialog(){
        val dialogTitle= getString(R.string.name_of_list);
        val positiveButtonTitle= getString(R.string.create_list);

        val builder = AlertDialog.Builder(this);
        val listTitleEditText = EditText(this);
        listTitleEditText.inputType= InputType.TYPE_CLASS_TEXT;

        builder.setTitle(dialogTitle);
        builder.setView(listTitleEditText);

        builder.setPositiveButton(positiveButtonTitle){dialog, i ->
            val list = listTitleEditText.text.toString();
            ref.child(listId).child("list-item").setValue(list);
            dialog.dismiss();
        }

        builder.create().show();
    }









}
